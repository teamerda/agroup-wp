<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package bee
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_sidebar( 'footerfull' ); ?>

<div class="wrapper bg-dark text-white" id="wrapper-footer">
	<div class="<?php echo esc_attr( $container ); ?>" id="contacts">
		<div class="row">
			<div class="col-12">
				<footer class="site-footer container mt-5 mb-5" id="colophon">
					<div class="row">
						<div class="col-12 col-lg-3 col-sm-6">
							<h3 class="footer-heading text-white mb-5"><?= pll__('Contact us') ?></h3>
							<p><span class="footer-icon-container text-center"><i class="fas fa-map-marker-alt"></i></span><a class="footer-link text-white" target="_blank" href="https://goo.gl/maps/nYmzeDFi34t"> <?= pll__('Duntes Str. 3 Riga, LV-1013, Latvia') ?></a></p>
							<p><span class="footer-icon-container text-center"><i class="fas fa-envelope"></i></span><a class="footer-link text-white" target="_blank" href="mailto:info@agroup.lv"> info@agroup.lv</a></p>
							<p><span class="footer-icon-container text-center"><i class="fas fa-phone"></i></span><a class="footer-link text-white" target="_blank" href="tel:+371 (6) 789-5602">+371 (6) 789-5602</a></p>
						</div>
						<div class="col-12 col-lg-3 col-sm-6">
							<h3 class="footer-heading text-white mb-5"><?= pll__('Social media links') ?></h3>
							<a class="text-white h4 mr-2" target="_blank" href="https://www.facebook.com/agroup.lv/"><i class="fab fa-facebook-square"></i></a>
							<a class="text-white h4" target="_blank" href="https://www.linkedin.com/company/453512/"><i class="fab fa-linkedin"></i></a>
						</div>
						<div class="col-12 col-lg-6 d-none d-lg-block">
							<a href="#"><h3 class="footer-heading text-white mb-5"><?= pll__('Home') ?></h3></a>
							<?php wp_nav_menu(
								array(
									'theme_location'  => 'primary',
									'container_class' => '',
									'container_id'    => 'navbarNavDropdown',
									'menu_class'      => 'navbar-nav footer-nav',
									'fallback_cb'     => '',
									'menu_id'         => 'footer-menu',
									'walker'          => new understrap_WP_Bootstrap_Navwalker(),
								)
							); ?>
						</div>
					</div>
				</footer><!-- #colophon -->
			</div><!--col end -->
		</div><!-- row end -->
	</div><!-- container end -->
</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>
