<?php
/**
 * Understrap functions and definitions
 *
 * @package understrap
 */

/**
 * Initialize theme default settings
 */
require get_template_directory() . '/inc/theme-settings.php';

/**
 * Theme setup and custom theme supports.
 */
require get_template_directory() . '/inc/setup.php';

/**
 * Register widget area.
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Enqueue scripts and styles.
 */
require get_template_directory() . '/inc/enqueue.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/pagination.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom Comments file.
 */
require get_template_directory() . '/inc/custom-comments.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load custom WordPress nav walker.
 */
require get_template_directory() . '/inc/bootstrap-wp-navwalker.php';

/**
 * Load WooCommerce functions.
 */
require get_template_directory() . '/inc/woocommerce.php';

/**
 * Load Editor functions.
 */
require get_template_directory() . '/inc/editor.php';

function wpb_widgets_init() {

    register_sidebar( array(
        'name'          => 'Under Infographics Widget Area',
        'id'            => 'under-infographics-widget',
        'before_widget' => '<div class="infographics-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => '',
    ) );

    register_sidebar( array(
        'name'          => 'Request Demo Widget Area',
        'id'            => 'request-demo-widget',
    ) );

    register_sidebar( array(
        'name'          => 'Partners Logos',
        'id'            => 'partners',
        'before_widget' => '<div class="partners-list">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => '',
    ) );

    register_sidebar( array(
        'name'          => 'Corporate Logos',
        'id'            => 'corporate',
        'before_title'  => '',
        'after_title'   => '',
    ) );

    register_sidebar( array(
        'name'          => 'HR providers Logos',
        'id'            => 'providers',
        'before_title'  => '',
        'after_title'   => '',
    ) );
}
add_action( 'widgets_init', 'wpb_widgets_init' );


/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function contactform_hook_js_footer() {
    ?>
        <script>
            document.addEventListener( 'wpcf7mailsent', function( event ) {
                if ( '2073' == event.detail.contactFormId  || '2072' == event.detail.contactFormId) {
                    var url = 'http://www.agroup.lv/wp-content/uploads/2019/01/Agroup-presentation-2.pdf'
                    window.open(url, '_blank', 'location=yes');
                    
                }
            }, false );
        </script>
    <?php
}
add_action('wp_footer', 'contactform_hook_js_footer');

// pll_register_string( $name, $string, $group, $multiline );
// ‘$name’ => (required) name provided for sorting convenience (ex: ‘myplugin’)
// ‘$string’ => (required) the string to translate
// ‘$group’ => (optional) the group in which the string is registered, defaults to ‘polylang’
// ‘$multiline’ => (optional) if set to true, the translation text field will be multiline, defaults to false
pll_register_string('Main title', 'HR software can be your powerful tool');
pll_register_string('Main subtitle', 'ensuring legislation compliance, cost optimization and revenue increase');
pll_register_string('"Request demo" button', 'Request demo');
pll_register_string('"Download PDF" button', 'Download PDF');
pll_register_string('HRB Portal subtitle', 'Simple Management%sof Complex Workforce');
pll_register_string('Title "Modules"', 'Modules');
pll_register_string('"Watch video" button', 'Watch video');
pll_register_string('"Payroll" title', 'Payroll');
pll_register_string('Adaptive calculation', 'Adaptive calculation');
pll_register_string('Payroll control reports', 'Payroll control reports');
pll_register_string('Complete set of tax...', 'Complete set of tax and statistical reports according to legal requirements');
pll_register_string('Configurable interfaces...', 'Configurable interfaces with legacy systems and legal authorities %s');
pll_register_string('"Core HR" title', 'Core HR');
pll_register_string('Employee record maintenance', 'Employee record maintenance');
pll_register_string('Personnel administration...', 'Personnel administration (incl. hiring/onboarding, contract changes, contract terminations/liability clearance)');
pll_register_string('Organisational structure and position management', 'Organisational structure and position management');
pll_register_string('HR compliance (policies, legal reporting)', 'HR compliance (policies, legal reporting)');
pll_register_string('Time & attendance', 'Time &amp; attendance');
pll_register_string('Extensible time types definition', 'Extensible time types definition');
pll_register_string('Flexible working time schedules and timesheets definition', 'Flexible working time schedules and timesheets definition');
pll_register_string('Adaptive time validation rules', 'Adaptive time validation rules');
pll_register_string('Interfacing with clocking systems, captured raw time cleansing', 'Interfacing with clocking systems, captured raw time cleansing');
pll_register_string('Absence registration and management', 'Absence registration and management');
pll_register_string('Performance evaluations', 'Performance %sevaluations');
pll_register_string('Manager-subordinate goals...', 'Manager-subordinate goals contracting for performance evaluation period');
pll_register_string('Goals cascading and alignment', 'Goals cascading and alignment');
pll_register_string('Goals execution monitoring', 'Goals execution monitoring');
pll_register_string('Performance review', 'Performance review');
pll_register_string('"Fleet management" title', 'Fleet management');
pll_register_string('Corporate vehicle register...', 'Corporate vehicle register (incl. responsible person, fuel type, average fuel consumption)');
pll_register_string('Itinerary lists', 'Itinerary lists (incl. travelled distance, average fuel consumption)');
pll_register_string('Consumed fuel reconciliation', 'Consumed fuel reconciliation based on responsible person’s reports vs merchant’s reports');
pll_register_string('"Advanced Workforce Scheduler" title', 'Advanced Workforce%sScheduler');
pll_register_string('Workplaces skill-based profiling', 'Workplaces skill-based profiling');
pll_register_string('Employees skill-based profiling', 'Employees skill-based profiling');
pll_register_string('Forecast based workplaces working time scheduling', 'Forecast based workplaces working time scheduling');
pll_register_string('Optimal employees scheduling...', 'Optimal employees scheduling by workplaces based on profiles matching analysis, employee availability and workplaces working time schedules');
pll_register_string('"Social networking tools" title', 'Social networking %stools');
pll_register_string('Enables teamwork efficiency...', 'Enables teamwork efficiency across departments and geographies');
pll_register_string('Facilitates performance reviews...', 'Facilitates performance reviews, recruiting and other processes');
pll_register_string('Improves employee development', 'Improves employee development and learning by building communities around professional leaders');
pll_register_string('"Benefits administration" title', 'Benefits %sadministration');
pll_register_string('Benefit plans administration', 'Benefit plans administration');
pll_register_string('Integration with HR and Payroll', 'Integration with HR and Payroll');
pll_register_string('Expenses control', 'Expenses control');
pll_register_string('Reporting and statistical analysis', 'Reporting and statistical analysis');
pll_register_string('"Recruitment" title', 'Recruitment');
pll_register_string('Recruitment requests management', 'Recruitment requests management');
pll_register_string('Candidates resumes and applications...', 'Candidates resumes and applications registration, including through self service portal');
pll_register_string('Recruiting events management...', 'Recruiting events management (interviews, testing sessions, etc)');
pll_register_string('Integration with recruiting agencies...', 'Integration with recruiting agencies, using industry standards like HR-XML');
pll_register_string('"Learning & sdevelopment" title', 'Learning & %sdevelopment');
pll_register_string('Training courses...', 'Training courses management, including training content');
pll_register_string('Training events management...', 'Training events management, including e-learning and class based trainings');
pll_register_string('Post training testing...', 'Post training testing (certification)');
pll_register_string('Integration with course contents providers...', 'Integration with course contents providers, using industry standards like SCORM and others');
pll_register_string('Business trips & Expense reports Title', 'Business trips & %sExpense reports');
pll_register_string('Business travel requests', 'Business travel requests (incl. transportation and accomodation booking, travel desk involvement)');
pll_register_string('Business travel reports', 'Business travel reports');
pll_register_string('Expense reports processing', 'Expense reports processing');
pll_register_string('Payroll', 'Payroll');
pll_register_string('Core HR', 'Core HR');
pll_register_string('Time & attendance', 'Time & attendance');
pll_register_string('Performance evaluations', 'Performance evaluations');
pll_register_string('Fleet management', 'Fleet management');
pll_register_string('Social networking tools', 'Social networking tools');
pll_register_string('Benefits administration', 'Benefits administration');
pll_register_string('Recruitment', 'Recruitment');
pll_register_string('Learning & development', 'Learning & development');
pll_register_string('Business trips & Expense reports', 'Business trips & Expense reports');
pll_register_string('Advanced workforce scheduler', 'Advanced workforce scheduler');
pll_register_string('Our clients', 'Our clients');
pll_register_string('Corporate companies', 'Corporate companies');
pll_register_string('HR services providers', 'HR services providers');
pll_register_string('Flexibly manage and coordinate employees’ team work', 'Flexibly manage and coordinate employees’ team work');
pll_register_string('Use of HRB Portal enables', 'Use of HRB %sPortal enables');
pll_register_string('Remote education of employees and testing of the knowledge acquired', 'Remote education of employees and testing of the knowledge acquired');
pll_register_string('End-to-end automation of HR processes...', 'End-to-end automation of HR processes for different offices of the company taking into account local specifics of various geographies');
pll_register_string('Use of HRB Portal enables', 'Use of HRB %sPortal enables');
pll_register_string('Obtain additional revenue stream...', 'Obtain additional revenue stream of recurring revenue from the provision of platform usage rights');
pll_register_string('Increase speed and relevance of provided information', 'Increase speed and relevance of provided information');
pll_register_string('Increase the bundle of provided services and contact points with clients', 'Increase the bundle of provided services and contact points with clients');
pll_register_string('Testimonials', 'Testimonials');
pll_register_string('Testemonial 1', 'There is definitively a decrease in paperwork. If we needed to make lots of documents manually previously, now we don’t have a need for that. We relocated these resources and now were able to focus on other things.');
pll_register_string('Robert Bosch SIA', 'Robert Bosch SIA');
pll_register_string("Olga Pleyer - Controlling, Finance and Administration Baltic's", "Olga Pleyer - Controlling, Finance and Administration Baltic's");
pll_register_string('Testemonial 2', 'The system is very user friendly. We didn’t need to train employees on how to use it or send long emails with process descriptions. Everything is simple and intuitive.');
pll_register_string('Testemonial 3', 'HRB Portal saves resources of HR and lets company work paperless. We achieved our dream of paperless HR.');
pll_register_string('Skirmantė Juknė, Telia Lithuania', 'Skirmantė Juknė, Telia Lithuania');
pll_register_string('Previous', 'Previous');
pll_register_string('Next', 'Next');
pll_register_string('Partners', 'Partners');
pll_register_string('Latest news', 'Latest news');
pll_register_string('Contact us', 'Contact us');
pll_register_string('Social media links', 'Social media links');
pll_register_string('Home', 'Home');
pll_register_string('Duntes Str. 3 Riga, LV-1013, Latvia', 'Duntes Str. 3 Riga, LV-1013, Latvia');
pll_register_string('Home', 'Home');
pll_register_string('More', 'More');
pll_register_string('Scroll down to learn more', 'Scroll down to learn more');
pll_register_string('How HRB companies%soutperform market', 'How HRB companies%soutperform market');
pll_register_string('Infographic 1 PNG', '/img/infographics/infog1.png');
pll_register_string('Infographic 2 PNG', '/img/infographics/infog2.png');
pll_register_string('Infographic 3 PNG', '/img/infographics/infog3.png');
pll_register_string('Infographic 1 SVG', '/img/infographics/infog1.svg');
pll_register_string('Infographic 2 SVG', '/img/infographics/infog2.svg');
pll_register_string('Infographic 3 SVG', '/img/infographics/infog3.svg');
