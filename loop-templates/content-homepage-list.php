<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

?>

<article <?php post_class('col-12 col-md-6 col-xl-3'); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">
		<a href="<?php echo esc_url(get_permalink()) ?>"><div class="post-thumbnail" style="background-image: url('<?php echo get_the_post_thumbnail_url($post->ID, 'large' ) ?>')"></div></a>
		<?php the_title( sprintf( '<h2 class="entry-title mt-4 mb-2"><a href="%s" rel="bookmark">', esc_url(get_permalink())),
		'</a></h2>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">

		<?php
		the_excerpt();
		?>

		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
			'after'  => '</div>',
		) );
		?>

	</div><!-- .entry-content -->

</article><!-- #post-## -->
