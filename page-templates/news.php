<?php
/**
 * Template Name: News List Template
 *
 * Template used to display all blog posts.
 *
 * @package understrap
 */

get_header();
$container   = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper news-page" id="index-wrapper">

	<header class="entry-header page-after-header-slice">
		<div class="container">
			<?php the_title( sprintf( '<h2 class="entry-title mt-5"><a class="text-white" href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
			'</a></h2>' ); ?>
		</div>
	</header><!-- .entry-header -->

	<div class="container" id="content" tabindex="-1">
			<main class="site-main" id="main">
				<div class="row mt-4">
					<?php
						$wp_query = new WP_Query([
							'post_type' => 'post',
							'posts_per_page' => 6,
							'paged' => $paged
						]);
						$wp_query->query('showposts=6'.'&paged='.$paged);  ?>

					<?php if ( $wp_query->have_posts() ) : ?>
						<?php /* Start the Loop */ ?>

						<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

							<?php
							/*
							* Include the Post-Format-specific template for the content.
							* If you want to override this in a child theme, then include a file
							* called content-___.php (where ___ is the Post Format name) and that will be used instead.
							*/
							get_template_part( 'loop-templates/content-archive' );
							?>
						<?php endwhile; ?>

					<?php else : ?>

						<?php //get_template_part( 'loop-templates/content', 'none' ); ?>

					<?php endif; ?>
				</div><!-- .row -->
			</main><!-- #main -->

			<!-- The pagination component -->
			<?php understrap_pagination(); ?>

		<!-- Do the right sidebar check -->
		<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>



</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
