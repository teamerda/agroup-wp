<?php

/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();

$container = get_theme_mod('understrap_container_type');
?>

<?php if (is_front_page() && is_home()) : ?>
    <?php get_template_part('global-templates/hero'); ?>
<?php endif; ?>

<div class="wrapper" id="index-wrapper">
    <div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">
        <div class="row">
            <main class="site-main" id="main">
                <div class="jumbotron">
                    <div class="jumbotron-title-container">
                        <h1 class="jumbotron-title ml-lg-5 mb-4 pl-md-5"><?= pll__('HR software can be your powerful tool') ?></h1>
                        <h2 class="jumbotron-subtitle ml-lg-5 pl-md-5 font-weight-normal"><?= pll__('ensuring legislation compliance, cost optimization and revenue increase') ?></h2>
                        <div class="button-container ml-lg-5 mt-5 pl-md-5  text-center text-md-left">
                            <button class="btn btn-outline-light text-dark demo text-uppercase py-2 mb-2"
                                    data-toggle="modal"
                                    data-target="#request-demo"
                                    onClick="gtag('send', 'pageview', 'Request demo');">
                                    <?= pll__('Request demo') ?>
                            </button>
                            <a href="#features" class="btn btn-outline-light text-dark pdf text-uppercase py-2 mb-2"
                            onClick="gtag('send', 'pageview', 'Download PDF');"
                            >
                            <?= pll__('Download PDF') ?>
                            </a>
                        </div>
                    </div>
                    <a href="#hrb-portal"
                       class="scroll-link" data-content="<?= pll__('Scroll down to learn more') ?>"><?php echo get_template_part('img/inline/scroll.svg'); ?></a>
                </div>
                <div class="container-fluid">
                    <div id="hrb-portal" style="position: relative; top: -75px;"></div>
                    <div class="row hrb-container mt-4 mb-3">
                        <img class="hrb-logo" style="max-width: 220px" src="<?php echo get_bloginfo('template_url') ?>/img/hrb-logo.jpg"/>
                    </div>
                    <h3 class="font-weight-normal text-center page-subtitle"><?= sprintf(pll__('Simple Management%sof Complex Workforce'), '<br/>') ?>
                    </h3>
                    <section class="section-modules">
                        <div class="row text-center">
                        <button data-toggle="modal" data-target="#videoModal" class="btn btn-outline-light text-uppercase py-2 mb-2 mx-auto" onClick="gtag('send', 'pageview', 'Watch video');">><?= pll__('Watch video') ?></button>
                        </div>
                        <div class="row slice">
                            <div class="container">
                                <div class="row">
                                    <h2 class="subtitle text-uppercase text-white px-3 px-sm-0 pb-0"><?= pll__('Modules') ?></h2>
                                </div>
                            </div>
                        </div>
                        <div class="row bg-yellow">
                            <div class="container">
                                <div class="row mt-4 mb-5">
                                    <div class="module-description-list text-darkcol-12 col-lg-4 pb-4">
                                        <div id="payroll-description" class="module-description active">
                                            <h3 class="font-weight-normal text-uppercase"><?= pll__('Payroll') ?></h3>
                                            <ul class="module-description-item-list">
                                                <li class="module-description-item"><?= pll__('Adaptive calculation') ?></li>
                                                <li class="module-description-item"><?= pll__('Payroll control reports') ?></li>
                                                <li class="module-description-item"><?= pll__('Complete set of tax and statistical reports according to legal requirements') ?>
                                                </li>
                                                <li class="module-description-item"><?= sprintf (pll__('Configurable interfaces with legacy systems and legal authorities %s'), '<br/>')  ?>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="hr-description" class="module-description">
                                            <h3 class="font-weight-normal text-uppercase"><?= pll__('Core HR') ?></h3>
                                            <ul class="module-description-item-list">
                                                <li class="module-description-item"><?= pll__('Employee record maintenance') ?></li>
                                                <li class="module-description-item"><?= pll__('Personnel administration (incl. hiring/onboarding, contract changes, contract terminations/liability clearance)') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Organisational structure and position management') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('HR compliance (policies, legal reporting)') ?>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="time-description" class="module-description">
                                            <h3 class="font-weight-normal text-uppercase"><?= pll__('Time &amp; attendance') ?></h3>
                                            <ul class="module-description-item-list">
                                                <li class="module-description-item"><?= pll__('Extensible time types definition') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Flexible working time schedules and timesheets definition') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Adaptive time validation rules') ?></li>
                                                <li class="module-description-item"><?= pll__('Interfacing with clocking systems, captured raw time cleansing') ?>
                                                </li>

                                                <li class="module-description-item"><?= pll__('Absence registration and management') ?>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="performance-description" class="module-description">
                                            <h3 class="font-weight-normal text-uppercase"><?= sprintf (pll__('Performance %sevaluations'), '<br/>') ?>
                                            </h3>
                                            <ul class="module-description-item-list">
                                                <li class="module-description-item"><?= pll__('Manager-subordinate goals contracting for performance evaluation period') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Goals cascading and alignment') ?></li>
                                                <li class="module-description-item"><?= pll__('Goals execution monitoring') ?></li>
                                                <li class="module-description-item"><?= pll__('Performance review') ?></li>
                                            </ul>
                                        </div>
                                        <div id="fleet-description" class="module-description">
                                            <h3 class="font-weight-normal text-uppercase"><?= pll__('Fleet management') ?></h3>
                                            <ul class="module-description-item-list">
                                                <li class="module-description-item"><?= pll__('Corporate vehicle register (incl. responsible person, fuel type, average fuel consumption)') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Itinerary lists (incl. travelled distance, average fuel consumption)') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Consumed fuel reconciliation based on responsible person’s reports vs merchant’s reports') ?>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="scheduler-description" class="module-description">
                                            <h3 class="font-weight-normal text-uppercase"><?= sprintf(pll__('Advanced Workforce%sScheduler'), '<br>') ?></h3>
                                            <ul class="module-description-item-list">
                                                <li class="module-description-item"><?= pll__('Workplaces skill-based profiling') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Employees skill-based profiling') ?></li>
                                                <li class="module-description-item"><?= pll__('Forecast based workplaces working time scheduling') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Optimal employees scheduling by workplaces based on profiles matching analysis, employee availability and workplaces working time schedules') ?>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="social-description" class="module-description">
                                            <h3 class="font-weight-normal text-uppercase"><?= sprintf(pll__('Social networking %stools'), '<br/>') ?>
                                            </h3>
                                            <ul class="module-description-item-list">
                                                <li class="module-description-item"><?= pll__('Enables teamwork efficiency across departments and geographies') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Facilitates performance reviews, recruiting and other processes') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Improves employee development and learning by building communities around professional leaders') ?>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="benefits-description" class="module-description">
                                            <h3 class="font-weight-normal text-uppercase"><?= sprintf(pll__('Benefits %sadministration'), '<br/>') ?>
                                            </h3>
                                            <ul class="module-description-item-list">
                                                <li class="module-description-item"><?= pll__('Benefit plans administration') ?></li>
                                                <li class="module-description-item"><?= pll__('Integration with HR and Payroll') ?></li>
                                                <li class="module-description-item"><?= pll__('Expenses control') ?></li>
                                                <li class="module-description-item"><?= pll__('Reporting and statistical analysis') ?>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="recruitment-description" class="module-description">
                                            <h3 class="font-weight-normal text-uppercase"><?= pll__('Recruitment') ?></h3>
                                            <ul class="module-description-item-list">
                                                <li class="module-description-item"><?= pll__('Recruitment requests management') ?></li>
                                                <li class="module-description-item"><?= pll__('Candidates resumes and applications registration, including through self service portal') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Recruiting events management (interviews, testing sessions, etc)') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Integration with recruiting agencies, using industry standards like HR-XML') ?>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="learning-description" class="module-description">
                                            <h3 class="font-weight-normal text-uppercase"><?= sprintf(pll__('Learning & %sdevelopment'), '<br/>') ?></h3>
                                            <ul class="module-description-item-list">
                                                <li class="module-description-item"><?= pll__('Training courses management, including training content') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Training events management, including e-learning and class based trainings') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Post training testing (certification)') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Integration with course contents providers, using industry standards like SCORM and others') ?>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="business-description" class="module-description">
                                            <h3 class="font-weight-normal text-uppercase"><?= sprintf(pll__('Business trips & %sExpense reports'), '<br/>')  ?></h3>
                                            <ul class="module-description-item-list">
                                                <li class="module-description-item"><?= pll__('Business travel requests (incl. transportation and accomodation booking, travel desk involvement)') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Business travel reports') ?>
                                                </li>
                                                <li class="module-description-item"><?= pll__('Expense reports processing') ?>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                    <div class="module-list col-12 col-lg-8 text-center text-lg-left">
                                        <div id="payroll-card" class="module-card active">
                                            <div class="module-icon"><?php echo get_template_part('img/inline/modules/payroll.svg'); ?></div>
                                            <div class="module-label"><?= pll__('Payroll') ?></div>
                                        </div>
                                        <div id="hr-card" class="module-card">
                                            <div class="module-icon"><?php echo get_template_part('img/inline/modules/core-hr.svg'); ?></div>
                                            <div class="module-label"><?= pll__('Core HR') ?></div>
                                        </div>
                                        <div id="time-card" class="module-card">
                                            <div class="module-icon"><?php echo get_template_part('img/inline/modules/time.svg'); ?></div>
                                            <div class="module-label"><?= pll__('Time & attendance') ?></div>
                                        </div>
                                        <div id="performance-card" class="module-card">
                                            <div class="module-icon"><?php echo get_template_part('img/inline/modules/performance.svg'); ?></div>
                                            <div class="module-label"><?= pll__('Performance evaluations') ?></div>
                                        </div>
                                        <div id="fleet-card" class="module-card">
                                            <div class="module-icon"><?php echo get_template_part('img/inline/modules/fleet.svg'); ?></div>
                                            <div class="module-label"><?= pll__('Fleet management') ?></div>
                                        </div>
                                        <div id="social-card" class="module-card">
                                            <div class="module-icon"><?php echo get_template_part('img/inline/modules/social.svg'); ?></div>
                                            <div class="module-label"><?= pll__('Social networking tools') ?></div>
                                        </div>
                                        <div id="benefits-card" class="module-card">
                                            <div class="module-icon"><?php echo get_template_part('img/inline/modules/benefits.svg'); ?></div>
                                            <div class="module-label"><?= pll__('Benefits administration') ?></div>
                                        </div>
                                        <div id="recruitment-card" class="module-card">
                                            <div class="module-icon"><?php echo get_template_part('img/inline/modules/recruitment.svg'); ?></div>
                                            <div class="module-label"><?= pll__('Recruitment') ?></div>
                                        </div>
                                        <div id="learning-card" class="module-card">
                                            <div class="module-icon"><?php echo get_template_part('img/inline/modules/learning.svg'); ?></div>
                                            <div class="module-label"><?= pll__('Learning & development') ?></div>
                                        </div>
                                        <div id="business-card" class="module-card">
                                            <div class="module-icon"><?php echo get_template_part('img/inline/modules/business.svg'); ?></div>
                                            <div class="module-label"><?= pll__('Business trips & Expense reports') ?></div>
                                        </div>
                                        <div id="scheduler-card" class="module-card">
                                            <div class="module-icon"><?php echo get_template_part('img/inline/modules/scheduler.svg'); ?></div>
                                            <div class="module-label"><?= pll__('Advanced workforce scheduler') ?></div>
                                        </div>
                                    </div>
                                    <div class="col-12 pl-lg-0 text-center text-lg-left">
                                        <button class="btn btn-white demo  px-4 text-uppercase" data-toggle="modal"
                                                data-target="#request-demo">
                                            <?= pll__('Request demo') ?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <script type="text/javascript">
                        jQuery(function ($) {
                            $('.module-card').click(function () {
                                $('.module-card').removeClass('active');
                                $(this).addClass('active');
                                $('.module-description').removeClass('active');
                                $('#' + this.id.split('-')[0] + '-description').addClass('active');

                                if ($(document).width() < 1000) {
                                    $('html, body').animate({
                                        scrollTop: $(".section-modules").offset().top - 20
                                    }, 450);
                                }
                            })
                        });
                    </script>
                    <section class="row section-market-performance bg-l-gray" id="features">
                        <div class="container text-center">
                            <h2 class="subtitle text-uppercase text-center"><?= sprintf(pll__('How HRB companies%soutperform market'), '<br/>') ?></h2>
                            <div class="infographick-container">
                                <img src="<?php echo get_bloginfo('template_url') ?><?= pll__('/img/infographics/infog1.svg') ?>"
                                     class="performance-infographics" alt="performance infographics 1"
                                     onerror="this.src='<?php echo get_bloginfo('template_url') ?><?= pll__('/img/infographics/infog1.png') ?>"/>
                                <img src="<?php echo get_bloginfo('template_url') ?><?= pll__('/img/infographics/infog2.svg') ?>"
                                     class="performance-infographics" alt="performance infographics 2"
                                     onerror="this.src='<?php echo get_bloginfo('template_url') ?><?= pll__('/img/infographics/infog2.png') ?>"/>
                                <img src="<?php echo get_bloginfo('template_url') ?><?= pll__('/img/infographics/infog3.svg') ?>"
                                     class="performance-infographics" alt="performance infographics 3"
                                     onerror="this.src='<?php echo get_bloginfo('template_url') ?><?= pll__('/img/infographics/infog3.png') ?>"/>
                            </div>
                            <div class="pdf-download-form my-5 pb-4">
                                <?php
                                if (is_active_sidebar('under-infographics-widget')) : ?>
                                    <div id="under-infographics-widget" class="chw-widget-area widget-area"
                                         role="complementary">
                                        <?php dynamic_sidebar('under-infographics-widget'); ?>
                                    </div>

                                <?php endif; ?>
                            </div>

                        </div>
                    </section>
                    <section class="row section-clients" id="clients">
                        <div class="container">
                            <h2 class="subtitle text-uppercase pb-2"><?= pll__('Our clients') ?></h2>
                            <div class="clients-content row">
                                <ul class="nav mb-3 w-100 justify-content-center" id="pills-tab" role="tablist">
                                    <li class="nav-item mt-4 mt-sm-0 ">
                                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill"
                                           href="#pills-home" role="tab" aria-controls="pills-home"
                                           aria-selected="true"><?= pll__('Corporate companies') ?></a>
                                    </li>
                                    <li class="nav-item mt-4 mt-sm-0 ">
                                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill"
                                           href="#pills-profile" role="tab" aria-controls="pills-profile"
                                           aria-selected="false"><?= pll__('HR services providers') ?></a>
                                    </li>
                                </ul>
                                <div class="tab-content col-12" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                                         aria-labelledby="pills-home-tab">
                                        <div class="row">
                                            <div class="col-12 col-lg-4">
                                                <div class="enable-list-container">
                                                    <p class="h5 font-weight-light text-uppercase"><?= sprintf(pll__('Use of HRB %sPortal enables'), '<br/>') ?>:</p>
                                                    <ul class="mb-5 clients-ul pl-0">
                                                        <li class="pb-2"><?= pll__('Flexibly manage and coordinate employees’ team work') ?>
                                                        </li>
                                                        <li class="pb-2"><?= pll__('Remote education of employees and testing of the knowledge acquired') ?>
                                                        </li>
                                                        <li class="pb-2"><?= pll__('End-to-end automation of HR processes for different offices of the company taking into account local specifics of various geographies') ?>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-8 logo-gallery">
                                                <?php if (is_active_sidebar('corporate')) : ?>
                                                    <?php dynamic_sidebar('corporate'); ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                                         aria-labelledby="pills-profile-tab">
                                        <div class="row">
                                            <div class="col-12 col-lg-4">
                                                <div class="enable-list-container">
                                                    <p class="h5 font-weight-light text-uppercase"><?= sprintf(pll__('Use of HRB %sPortal enables'), '<br/>') ?>:</p>
                                                    <ul class="mb-5 clients-ul pl-0">
                                                        <li class="pb-2"><?= pll__('Obtain additional revenue stream of recurring revenue from the provision of platform usage rights') ?>
                                                        </li>
                                                        <li class="pb-2"><?= pll__('Increase speed and relevance of provided information') ?>
                                                        </li>
                                                        <li class="pb-2"><?= pll__('Increase the bundle of provided services and contact points with clients') ?>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-8 logo-gallery">
                                                <?php if (is_active_sidebar('providers')) : ?>
                                                    <?php dynamic_sidebar('providers'); ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="row section-testemonials section-mb bg-d-gray text-white" id="testemonials">
                        <div class="container">
                            <h2 class="subtitle text-warning text-uppercase"><?= pll__('Testimonials') ?></h2>
                            <div id="testemonials-carusel" class="testemonials-carusel carousel slide"
                                 data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <p class="testemonial-quote">&#xab;<?= pll__('There is definitively a decrease in paperwork. If we needed to make lots of documents manually previously, now we don’t have a need for that. We relocated these resources and now were able to focus on other things.') ?>&#xbb;</p>
                                        <p class="testemonial-author text-right"><?= pll__('Robert Bosch SIA') ?><br/><?= pll__("Olga Pleyer - Controlling, Finance and Administration Baltic's") ?></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="testemonial-quote">&#xab;<?= pll__('The system is very user friendly. We didn’t need to train employees on how to use it or send long emails with process descriptions. Everything is simple and intuitive.') ?>&#xbb;</p>
                                        <p class="testemonial-author text-right"><?= pll__('Robert Bosch SIA') ?><br/><?= pll__("Olga Pleyer - Controlling, Finance and Administration Baltic's") ?></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="testemonial-quote">&#xab;<?= pll__('HRB Portal saves resources of HR and lets company work paperless. We achieved our dream of paperless HR.') ?>&#xbb;</p>
                                        <p class="testemonial-author text-right"><?= pll__('Skirmantė Juknė, Telia Lithuania') ?></p>
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#testemonials-carusel" role="button"
                                   data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only"><?= pll__('Previous') ?></span>
                                </a>
                                <a class="carousel-control-next" href="#testemonials-carusel" role="button"
                                   data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only"><?= pll__('Next') ?></span>
                                </a>
                            </div>
                        </div>
                        <div class="testemonials-slice container-fluid"></div>
                    </section>
                    <section class="row section-partners">
                        <div class="container">
                            <h2 class="subtitle text-uppercase"><?= pll__('Partners') ?></h2>
                            <?php
                            if (is_active_sidebar('partners')) : ?>
                                <div id="partners" class="chw-widget-area widget-area" role="complementary">
                                    <?php dynamic_sidebar('partners'); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </section>
                    <section class="row section-latest-news bg-l-gray pb-5" id="blog">
                        <div class="container">
                            <h2 class="subtitle text-center mt-5 mb-0 text-uppercase"><?= pll__('Latest news') ?></h2>
                            <div class="row post-list">
                                <?php
                                // the query
                                $post_query = new WP_Query(array(
                                    'posts_per_page' => 4,
                                ));
                                ?>
                                <?php if ($post_query->have_posts()) : ?>
                                    <?php /* Start the Loop */ ?>
                                    <?php while ($post_query->have_posts()) : $post_query->the_post(); ?>
                                        <?php
                                        /*
                                         * Include the Post-Format-specific template for the content.
                                         * If you want to override this in a child theme, then include a file
                                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                         */
                                        get_template_part('loop-templates/content', 'homepage-list');
                                        ?>
                                    <?php endwhile; ?>
                                <?php else : ?>
                                    <?php get_template_part('loop-templates/content', 'none'); ?>
                                <?php endif; ?>
                            </div>
                            <div class="more-news text-center">
                                <a href="<?php echo get_permalink(get_page_by_path('news')) ?>"
                                   class="btn btn-secondary text-white text-uppercase px-5 mt-4"><?= pll__('More') ?></a>
                            </div>
                        </div>
                    </section>
                </div>
            </main><!-- #main -->
        </div><!-- #primary -->
    </div><!-- .row -->
    <div class="fixed-ribbons">
        <button class="btn btn-dark fixed-ribbon demo" data-toggle="modal" data-target="#request-demo" onClick="gtag('send', 'pageview', 'Request demo');"><?= pll__('Request demo') ?>
        </button>
        <button href="#features" id="features-link" class="btn btn-dark  fixed-ribbon pdf" onClick="gtag('send', 'pageview', 'Download PDF');"><?= pll__('Download PDF') ?></button>
        <button data-toggle="modal" data-target="#videoModal"class="btn btn-dark fixed-ribbon video" onClick="gtag('send', 'pageview', 'Watch video');"><?= pll__('Watch video') ?></button>
        <script type="text/javascript">
            jQuery(function ($) {
                $('#features-link').click(function () {
                    $('html, body').animate({
                        scrollTop: $("#features").offset().top - 20
                    }, 800);
                })
            });
        </script>
    </div>
    <div class="modal fade" id="request-demo" tabindex="-1" role="dialog" aria-labelledby="request-demo-label"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php
                    if (is_active_sidebar('request-demo-widget')) : ?>
                        <div id="request-demo-widget" class="chw-widget-area widget-area" role="complementary">
                            <?php dynamic_sidebar('request-demo-widget'); ?>
                        </div>

                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog vid" role="document">
            <div class="modal-content video-modal">
                <iframe src="https://www.youtube.com/embed/R6oyTLxGe8c?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div><!-- Container end -->
<script type="text/javascript">
    // Cache selectors
    jQuery(function ($) {
        var lastId,
            topMenu = $("#main-menu"),
            topMenuHeight = topMenu.outerHeight() + 1,
            // All list items
            menuItems = topMenu.find("a");

        menuItems.parent().removeClass("active");
        // Anchors corresponding to menu items
        var scrollItems = menuItems.map(function () {
            var item = $($(this).attr("href").split('/').slice(-1)[0]);
            if (item.length) {
                return item;
            }

            menuItems.parent().removeClass("active")
        });

// Bind click handler to menu items
// so we can get a fancy scroll animation
        menuItems.click(function (e) {
            if ($(this).attr("href").indexOf('#') !== -1) {
                e.preventDefault();
                $(".navbar-collapse").collapse('hide');
                var href = $(this).attr("href").split('/').slice(-1)[0];
                if (!href) {
                    $('html, body').stop().animate({scrollTop: 0}, 850);
                    return false;
                }
                var offsetTop = href === "#" ? 0 : $(href).offset().top - topMenuHeight + 1;
                $('html, body').stop().animate({
                    scrollTop: offsetTop
                }, 850);
            }
        });

// Bind to scroll
        $(window).scroll(function () {
            // Get container scroll position
            var fromTop = $(this).scrollTop() + topMenuHeight;

            // Get id of current scroll item
            var cur = scrollItems.map(function () {
                if ($(this).offset().top < fromTop)
                    return this;
            });
            // Get the id of the current element
            cur = cur[cur.length - 1];
            var id = cur && cur.length ? cur[0].id : "";

            if (lastId !== id && id) {
                lastId = id;
                // Set/remove active class
                menuItems.parent().removeClass("active")
                    .end().filter("[href*=" + id + "]").parent().addClass("active");
            } else if (lastId !== id) {
                menuItems.parent().removeClass("active").end().first().parent().addClass("active");
            }
        });
    })
</script>

<script type="text/javascript">
	document.addEventListener( 'wpcf7mailsent', function( event ) {
if ( '14' == event.detail.contactFormId ) {
    location = '/wp-content/uploads/2019/01/Agroup-presentation-2.pdf';
}}, false );
								</script>

</div><!-- Wrapper end -->

<?php get_footer(); ?>
